conky.config = {
    alignment = 'tr',
    background = true,
    use_xft = true,
    xftfont = '123:size=8',
    xftalpha = 0.01,
    update_interval = 2.0,
    total_run_times = 0,
    own_window = true,
    own_window_transparent = true,
    own_window_type = 'override',
    own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
    double_buffer = true,
    minimum_size = '250 5',
    maximum_width = 400,
    draw_shades = false,
    draw_outline = false,
    draw_borders = false,
    draw_graph_borders = false,
    default_color = 'white',
    default_shade_color = 'red',
    default_outline_color = 'green',
    no_buffers = false,
    gap_x = 30,
    gap_y = 0,
    uppercase = false,
    cpu_avg_samples = 2,
    net_avg_samples = 1,
    override_utf8_locale = true,
    use_spacer = 'yes',
    text_buffer_size = 256
}

conky.text = [[

    #${font openlogos:size=20}U${font Arial:size=20}${color Tan1}GNU${color Ivory}LINUX${font openlogos:size=20}t

    ${voffset -30}
    ${color ddd}
    ${font}
    ${font Arial:bold:size=10}${color Tan1}SYSTEM ${color DarkSlateGray} ${hr 2}
    $font${color ddd}$sysname $kernel $alignr $machine
    CPU Frequency $alignr${freq_g cpu0}Ghz
    Uptime $alignr${uptime}
    # File System $alignr${fs_type}

    ${font Arial:bold:size=10}${color Tan1}PROCESSORS ${color DarkSlateGray}${hr 2}
    $font${color ddd}CPU1  ${cpu cpu1}% ${cpubar cpu1}
    CPU2  ${cpu cpu2}% ${cpubar cpu2}

    ${font Arial:bold:size=10}${color Tan1}MEMORY ${color DarkSlateGray}${hr 2}
    $font${color ddd}MEM $alignc $mem / $memmax $alignr $memperc%
    $membar

    ${font Arial:bold:size=10}${color Tan1}HDD ${color DarkSlateGray}${hr 2}
    $font${color ddd}/ $alignc ${fs_used /} / ${fs_size /} $alignr ${fs_used_perc /}%
    ${fs_bar /}
    /mnt/win $alignc ${fs_used /mnt/win} / ${fs_size /mnt/win} $alignr ${fs_used_perc /mnt/win}%
    ${fs_bar /mnt/win}

    ${font Arial:bold:size=10}${color Tan1}TOP CPU ${color DarkSlateGray}${hr 2}
    ${color ddd}$font${top name 2}${alignr}${top cpu 2} %
    $font${top name 3}${alignr}${top cpu 3} %
    $font${top name 4}${alignr}${top cpu 4} %
    $font${top name 5}${alignr}${top cpu 5} %

    ${font Arial:bold:size=10}${color Tan1}TOP MEMORY ${color DarkSlateGray}${hr 2}
    ${color ddd}$font${top_mem name 2}${alignr}${top mem 2} %
    $font${top_mem name 3}${alignr}${top mem 3} %
    $font${top_mem name 4}${alignr}${top mem 4} %
    $font${top_mem name 5}${alignr}${top mem 5} %

    ${font Arial:bold:size=10}${color Tan2}NETWORK ${color DarkSlateGray}${hr 2}
    $font${color ddd}IP on wifi $alignr ${addr wlp4s0}
    $font${color ddd}IP public $alignr ${execi 10800 ~/bin/get-public-ip.sh}

    Down $alignr ${downspeed wlp4s0} kb/s
    Up $alignr ${upspeed wlp4s0} kb/s

    Downloaded: $alignr  ${totaldown wlp4s0}
    Uploaded: $alignr  ${totalup wlp4s0}

    # ${execi 3000 python2 weather.py}${voffset -4}${color1}${font Ubuntu:size=14}Forecast$color$font
    # ${voffset 4}${goto 18}${execi 3000 sed '1q;d' conkyweather.txt}${goto 88}${execi 3000 sed '2q;d' conkyweather.txt}${goto 158}${execi 3000 sed '3q;d' conkyweather.txt}${goto 228}${execi 3000 sed '4q;d' conkyweather.txt}
    # ${execi 3000 cp -f ./conky_icons/$(sed '5q;d' conkyweather.txt).png ~/.cache/weather-1.png}${image ~/.cache/weather-1.png -p 0,50 -s 30x30}${execi 3000 cp -f ./conky_icons/$(sed '6q;d' conkyweather.txt).png ~/.cache/weather-2.png}${image ~/.cache/weather-2.png -p 70,50 -s 30x30}${execi 3000 cp -f ./conky_icons/$(sed '7q;d' conkyweather.txt).png ~/.cache/weather-3.png}${image ~/.cache/weather-3.png -p 140,50 -s 30x30}${execi 3000 cp -f ./conky_icons/$(sed '8q;d' conkyweather.txt).png ~/.cache/weather-4.png}${image ~/.cache/weather-4.png -p 210,50 -s 30x30}${voffset 16}
    # ${goto 20}${execi 3000 sed '9q;d' conkyweather.txt}°${goto 90}${execi 3000 sed '10q;d' conkyweather.txt}°${goto 160}${execi 3000 sed '11q;d' conkyweather.txt}°${goto 230}${execi 3000 sed '12q;d' conkyweather.txt}°
    # ${goto 20}${execi 3000 sed '13q;d' conkyweather.txt}°${goto 90}${execi 3000 sed '14q;d' conkyweather.txt}°${goto 160}${execi 3000 sed '15q;d' conkyweather.txt}°${goto 230}${execi 3000 sed '16q;d' conkyweather.txt}°${voffset -13}

    # ${font Arial:bold:size=10}${color Tan2}WEATHER ${color DarkSlateGray}${hr 2}
    # ${font}${color ddd}

    # ${voffset -25}${font Weather:size=45}${execi 1800 conkyForecast –location=BEXX0008 –datatype=WF}
    # ${alignc 22}${voffset -60}${font Arial:bold:size=10}${color ddd}${execi 1800 conkyForecast –location=BEXX0008 –datatype=HT}
    # $font${voffset -55}${alignr}${color ddd}Wind: ${execi 1800 conkyForecast –location=BEXX0008 –datatype=WS}
    # ${alignr}${color ddd}Humidity: ${execi 1800 conkyForecast –location=BEXX0008 –datatype=HM}
    # ${alignr}${color ddd}Precipitation: ${execi 1800 conkyForecast –location=BEXX0008 –datatype=PC}

    # ${color ddd}Sunrise: $alignr${execi 1800 conkyForecast –location=BEXX0008 –datatype=SR}${alignr}
    # Sunset: $alignr${execi 1800 conkyForecast –location=BEXX0008 –datatype=SS}$color

    # ${font Arial:bold:size=10}${color Tan2}MUSIC ${color DarkSlateGray}${hr 2}
    # ${color ddd}$font${if_running mpd}
    # $mpd_smart
    # $mpd_album
    # Bitrate $mpd_bitrate kbits/s
    # $mpd_status $mpd_elapsed/$mpd_length
    # ${font Arial:bold:size=10}${color Tan2}TIME ${color DarkSlateGray}${hr 2}
    # 
    # ${color DarkSlateGray} ${font :size=30}$alignc${time %H:%Mh}
    # ${voffset -30}${font :bold:size=10}$alignc${time %d %b. %Y}
    # ${font :bold:size=8}$alignc${time %A}
    # $endif
]]
