#!/bin/sh

args=("$@")
VOLSCRIPT=${args[0]}volume
VOLUME=$(sh $VOLSCRIPT)
TEXT="$VOLUME"
COLOR="#096BAA"

HEADPHONES=$(amixer -c 0 contents | grep -A2 "'Headphone Playback Switch'" | grep "values=on")
if [[ -z $HEADPHONES ]]; then HEADPHONES=$(amixer -c 1 contents | grep -A2 "'Headphone Jack'" | grep "values=on"); fi

if [[ -z $HEADPHONES ]]; then
	OUTPUT="[S]"
else
	OUTPUT="[H]"
fi


if [ "$VOLUME" = "MUTE" ]; then
	ICON="\uf026"
	COLOR="#FF0000"
	TEXT="Mute"
else
	VOLUMEINT=${VOLUME::-1}
	if [ $VOLUMEINT -lt 50 ]; then
		ICON="\uf027"
	else
		ICON="\uf028"
	fi
fi

echo "<span color='$COLOR' font='FontAwesome'>"$(printf "$ICON")" </span>" "$TEXT $OUTPUT"
