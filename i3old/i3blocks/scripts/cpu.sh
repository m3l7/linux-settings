USAGE=$(mpstat 1 1 | awk '$3 ~ /CPU/ { for(i=1;i<=NF;i++) { if ($i ~ /%idle/) field=i } } $3 ~ /all/ { printf("%d",100 - $field) }')

if [ $USAGE -lt 40 ]; then
	COLOR=#096BAA
else
	COLOR=#FF0000
fi

if [ $USAGE -lt 10 ]; then USAGE=" $USAGE"; fi

echo "<span color='$COLOR' font='FontAwesome'>"$(printf "\uf004")" </span>" "$USAGE"%

