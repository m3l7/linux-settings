#!/bin/sh

COLOR="#FF0000"

CAPS=$(xset -q | grep "Caps Lock:   on")
if [[ ! -z $CAPS ]]; then echo "<span color='$COLOR' font='FontAwesome'>CAPSLOCK</span>"; fi
