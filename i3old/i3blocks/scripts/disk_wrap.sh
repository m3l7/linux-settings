#!/bin/sh 

args=("$@")               
MEMORYSCRIPT=${args[0]}memory
ICON="\uf0ae"

RAM=$(sh $MEMORYSCRIPT ram | sed -n 2p)
HOME=$(df -k /home --output=avail,target -BG | grep "/home" | awk '{print $1}')
ROOT=$(df -k / --output=avail,target -BG | grep "/" | awk '{print $1}')
DATI=$(df -k /mnt/dati --output=avail,target -BG | grep "/mnt" | awk '{print $1}')

if [ ${RAM::-1} -gt 79 ] || [ ${HOME::-1} -lt 2 ] || [ ${ROOT::-1} -lt 1 ] || [ ${DATI::-1} -lt 10 ]; then
	COLOR="#FF0000"	
else
	COLOR="#096BAA"
fi

echo "dati $DATI  ~ $HOME  / $ROOT"
