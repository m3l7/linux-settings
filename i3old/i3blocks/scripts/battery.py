#!/usr/bin/env python3
#
# A battery indicator blocklet script for i3blocks

from subprocess import check_output

status = check_output(['acpi'], universal_newlines=True)
state = status.split(": ")[1].split(", ")[0]
commasplitstatus = status.split(", ")
percentleft = int(commasplitstatus[1].rstrip("%\n"))

def color_new(percent):
    if percent < 25:
        return "#FF0000"
    return "#096BAA"
def icon(percent):
    if percent < 25:
        return "\uf243"
    if percent < 50:
        return "\uf242"
    if percent < 75:
        return "\uf241"
    return "\uf240"

FA_LIGHTNING = "<span font='FontAwesome'>\uf0e7</span>"
FA_PLUG = "<span color='#096BAA' font='FontAwesome'>\uf1e6</span>"
#FA_BATTERY = "<span size='large' color='{}' font='FontAwesome'>\uf240</span>".format(color_new(percentleft));
FA_BATTERY = "<span color='{}' font='FontAwesome'>{}</span>".format(color_new(percentleft), icon(percentleft));
#FA_BATTERY = "<span color='{}'>Battery</span>".format(color_new(percentleft));

fulltext = ''
timeleft = ""

if state == "Discharging":
    time = commasplitstatus[-1].split()[0]
    time = ":".join(time.split(":")[0:2])
    timeleft = " ({})".format(time)
    fulltext += "" + FA_BATTERY
else:
    fulltext += "" + FA_PLUG

def color(percent):
    return "#FFFFFF"
    if percent < 10:
        return "#FF0000"
    if percent < 20:
        return "#FF3300"
    if percent < 30:
        return "#FF6600"
    if percent < 40:
        return "#FF9900"
    if percent < 50:
        return "#FFCC00"
    if percent < 60:
        return "#FFFF00"
    if percent < 70:
        return "#FFFF33"
    if percent < 80:
        return "#FFFF66"
    return "#FFFFFF"

form =  ' <span color="{}">{}%</span>'
fulltext += form.format(color(percentleft), percentleft)
fulltext += timeleft

print(fulltext)
print(fulltext)
