#!/bin/sh

args=("$@")               
BWSCRIPT=${args[0]}bandwidth
SSID=$(iwgetid -r)
CONNECTION=""
ICON=""
COLOR="#096BAA"
BANDWIDTH="$(sh $BWSCRIPT)" || exit;

if [ $SSID != "--" ]; then
	ICON="\uf1eb"
	CONNECTION=$SSID
fi

echo "<span color='$COLOR' font='FontAwesome'>"$(printf "$ICON")" </span>" "$CONNECTION $BANDWIDTH"
