#!/bin/sh

CURRENT=$(setxkbmap -print | grep xkb_symbols | awk '{print $4}' | awk -F"+" '{print $2}')

if [ $(env | grep BLOCK_BUTTON) == "BLOCK_BUTTON=1" ]; then
	LAYOUTARR=(gb no it de)
	#LAYOUTARR=(${LAYOUTS//,/ })

	NEXT=0
	NEXTELEMENT=${LAYOUTARR[0]}
	for element in "${LAYOUTARR[@]}"; do
		if [ $NEXT -eq 1 ]; then
			NEXT=0
			NEXTELEMENT=$element
			break
		fi
        	if [ $element == $CURRENT ]; then NEXT=1; fi
    	done
	setxkbmap $NEXTELEMENT
	if [ $NEXTELEMENT == "de" ]; then setxkbmap -layout de -variant qwerty; fi
	xmodmap ~/.Xmodmap
fi

setxkbmap -print | grep xkb_symbols | awk '{print $4}' | awk -F"+" '{print $2}'
xmodmap ~/.Xmodmap




