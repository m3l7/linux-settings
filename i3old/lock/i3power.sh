#!/bin/bash
############################################################
#
############################################################
function lock {
	#~/.config/i3/lock/lock.sh
	/usr/sbin/scripts/lock.sh
}
case "$1" in
    lock)
        lock
        ;;
    logout)
        i3-msg exit
        ;;
    suspend)
        lock && "/bin/systemctl suspend"
        ;;
    reboot)
        /bin/systemctl reboot
        ;;
    poweroff)
        /bin/systemctl poweroff
        ;;
    *)
        echo "Usage: $0 {lock|logout|suspend|reboot|poweroff}"
        exit 2
esac

exit 0

