#!/usr/bin/env bash

icon="/usr/sbin/scripts/lock.png"
tmpbg='/tmp/screen.png'
resolution=$(xrandr  | grep \* | cut -d' ' -f4)

(( $# )) && { icon=$1; }

#scrot "$tmpbg"
convert -size "$resolution" canvas:black "$tmpbg"
#convert "$tmpbg" -scale 20% -scale 500% "$tmpbg"
convert "$tmpbg" "$icon" -gravity center -composite -matte "$tmpbg"
i3lock -d -u -i "$tmpbg"
