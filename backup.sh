#!/bin/bash
# Author : Gazeka74 <gazeka74@gmail.com>

WHEREAMI=$(pwd)

WHOAMI=$(whoami)

OPTIONS="Options:
    -h, --help      This help menu.
    -r, --restore   Restore the dotfiles to the correct location
    -b, --backup    Backup the dotfiles and git add/commit/push"

function backup {
    printf "\n===> BACKUPING FILES FOR THE USER : $WHOAMI\n\n"

    echo "=> backuping autostart scripts"
    mkdir -p $WHEREAMI/autostart-scripts/
    mkdir -p $WHEREAMI/autostart/
    cp ~/.config/autostart/* $WHEREAMI/autostart/
    cp ~/.config/autostart-scripts/* $WHEREAMI/autostart-scripts/

    echo "=> backuping autokey"
    cp -r ~/.config/autokey $WHEREAMI/

    echo "=> backuping applications .desktop"
    mkdir -p $WHEREAMI/applications/
    cp ~/.local/share/applications/* $WHEREAMI/applications/

    echo "=> backuping user bin"
    mkdir -p $WHEREAMI/bin/
    cp ~/bin/* $WHEREAMI/bin/

    echo "=> backuping zshrc"
    cp ~/.zshrc $WHEREAMI/.zshrc
    cp ~/.oh-my-zsh $WHEREAMI/

    echo "=> backuping kde configs"
    cp ~/.config/kwinrc $WHEREAMI/
    cp ~/.config/kglobalshortcutsrc $WHEREAMI/
    mkdir -p $WEREAMI/konsolekxmlgui5
    cp ~/.local/share/kxmlgui5/konsole/* $WEREAMI/konsolekxmlgui5/
    mkdir -p $WEREAMI/konsoleprofile
    cp ~/.local/share/konsole/* $WEREAMI/konsoleprofile/

    printf "=> pushing to git \n\n"

    git add .
    git commit -m "dotfiles backup AUTO COMMIT : $(date)"
    git push origin master
}

function restore {
    printf "\n===> RESTORING FILES FOR THE USER : $WHOAMI\n\n"

    echo "=> restoring i3"
    mkdir -p ~/.config/i3
    cp $WHEREAMI/i3/config ~/.config/i3/config

    echo "=> restoring polybar"
    mkdir -p ~/.config/polybar/
    cp $WHEREAMI/polybar/config ~/.config/polybar/config
    cp $WHEREAMI/polybar/master.conf ~/.config/polybar/master.conf
    cp $WHEREAMI/polybar/modules.conf ~/.config/polybar/modules.conf

    echo "=> restoring zshrc"
    cp $WHEREAMI/.zshrc ~/.zshrc

    echo "=> restoring termite"
    mkdir -p ~/.config/termite/
    cp $WHEREAMI/termite/config ~/.config/termite/config

    echo "=> restoring i3lock-fancy (require root rights)"
    sudo mkdir -p /usr/share/i3lock-fancy
    sudo cp -r $WHEREAMI/locker/usrshare/i3lock-fancy /usr/share/
    sudo cp $WHEREAMI/locker/i3lock-fancy /usr/bin/i3lock-fancy 

    echo "=> restoring the background"
    mkdir -p /home/$WHOAMI/Images/background/
    cp $WHEREAMI/background/background.png /home/$WHOAMI/Images/background/background.png

    echo "=> restoring rofi"
    sudo mkdir -p /usr/share/rofi/themes
    sudo cp -r $WHEREAMI/rofi/themes /usr/share/rofi/
    cp $WHEREAMI/rofi/config ~/.config/rofi/config
}

function print_usage {
    printf "\nHandler.sh : a small script to backup and restore dotfiles \n\nUsage:\n     $(basename $0) [options]\n\n$OPTIONS\n\n"
}

while true; do
    if [ -z "$1" ]; then
        print_usage ; exit 1
    fi
    case "$1" in
        -h|--help)
            print_usage ; exit 1 ;;
        -b|--backup)
            backup; exit 0;;
        -r|--restore)
            restore; exit 0;;
    esac
done





